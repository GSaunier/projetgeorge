package vue;

import engine.GamePainter;
import model.Jeu;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Horatiu Cirstea, Vincent Thomas
 *
 * afficheur graphique pour le game
 *
 */
public class Painter implements GamePainter {

	private Jeu jeu;

	/**
	 * appelle constructeur parentq
	 *
	 * @param jeu
	 *            le jeutest a afficher
	 */
	public Painter(Jeu jeu) {
		this.jeu = jeu;
    }

	/**
	 * methode  redefinie de Afficheur retourne une image du jeu
	 */
	@Override
	public void draw(BufferedImage im) {
		Graphics2D crayon = (Graphics2D) im.getGraphics();

		crayon.setColor(jeu.pikachu.couleur);
		crayon.fillRect(jeu.pikachu.x,jeu.pikachu.y,50,50);
	}

	@Override
	public int getWidth() {
		return 1000;
	}

	@Override
	public int getHeight() {
		return 1000;
	}

}
