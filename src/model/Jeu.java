package model;

import engine.Cmd;
import engine.Game;

public class Jeu implements Game {
    public pokemon pikachu = new pokemon();

    @Override
    public void evolve(Cmd userCmd) {
        pikachu.sedeplacer(userCmd);
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
