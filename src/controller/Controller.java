package controller;

import engine.Cmd;
import engine.GameController;
import model.Jeu;

import java.awt.event.KeyEvent;
import java.util.ArrayList;


/**
 * @author Horatiu Cirstea, Vincent Thomas
 *
 * controleur de type KeyListener
 *
 */
public class Controller implements GameController {

	/**
	 * commande en cours
	 */
	private Cmd input;
	private boolean attaque = false;

	/**
	 * construction du controleur par defaut le controleur n'a pas de commande
	 */
	public Controller() {
		input = Cmd.IDLE;
	}

	@Override
	/**
	 * met a jour les commandes en fonctions des touches appuyees
	 */
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyChar()) {
			case 'f':
			case 'F':
				input = Cmd.END;
				break;
			case 'q':
			case 'Q':
				input = Cmd.LEFT;
				break;
			case 'd':
			case 'D':
				input = Cmd.RIGHT;
				break;
			case 'Z':
			case 'z':
				input = Cmd.UP;
				break;
			case 's':
			case 'S':
				input = Cmd.DOWN;
				break;
		}
	}

	@Override
	/**
	 * met a jour les commandes quand le joueur relache une touche
	 */
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
			default:
				input = Cmd.IDLE;
		}
	}

	@Override
	/**
	 * Quand appuyer et relacher
	 */
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public Cmd getCommand() {
		return input;
	}
}
