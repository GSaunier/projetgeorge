import controller.Controller;
import engine.GameEngineGraphical;
import model.Jeu;
import vue.Painter;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Jeu j = new Jeu();

        Painter painter = new Painter(j);

        Controller controller = new Controller();

        GameEngineGraphical engine = new GameEngineGraphical(j,painter,controller);

        engine.run();
    }
}
